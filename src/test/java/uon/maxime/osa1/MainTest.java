package uon.maxime.osa1;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTest {

    private String getPathForResourceFile(String path) throws URISyntaxException {
        var resourcePath = getClass().getResource(path);
        if (resourcePath == null)
            throw new NullPointerException("MainTest::getPathForResourceFile: path is nullptr.");
        return Paths.get(resourcePath.toURI()).toString();
    }

    @Test
    void parseDataFile1() throws URISyntaxException, IOException {
        Main.InputData inputData = Main.parseInputFile(getPathForResourceFile("/datafile1.txt"));
        assertEquals(1, inputData.getDispatcherRunningTime());
        Queue<Main.Process> expectedProcesses = new LinkedList<>();
        expectedProcesses.add(new Main.Process("p1", 0, 10));
        expectedProcesses.add(new Main.Process("p2", 0, 1));
        expectedProcesses.add(new Main.Process("p3", 0, 2));
        expectedProcesses.add(new Main.Process("p4", 0, 1));
        expectedProcesses.add(new Main.Process("p5", 0, 5));
        assertEquals(1, inputData.getDispatcherRunningTime());
        assertEquals(expectedProcesses, inputData.getProcesses());
    }

    @Test
    void parseDataFile2() throws URISyntaxException, IOException {
        Main.InputData inputData = Main.parseInputFile(getPathForResourceFile("/datafile2.txt"));
        assertEquals(1, inputData.getDispatcherRunningTime());
        Queue<Main.Process> expectedProcesses = new LinkedList<>();
        expectedProcesses.add(new Main.Process("p1", 0, 10));
        expectedProcesses.add(new Main.Process("p2", 2, 1));
        expectedProcesses.add(new Main.Process("p3", 6, 2));
        expectedProcesses.add(new Main.Process("p4", 10, 1));
        expectedProcesses.add(new Main.Process("p5", 14, 5));
        assertEquals(1, inputData.getDispatcherRunningTime());
        assertEquals(expectedProcesses, inputData.getProcesses());
    }

    @Test
    void testFcfs1() throws URISyntaxException, IOException {
        Main.InputData inputData = Main.parseInputFile(getPathForResourceFile("/datafile1.txt"));
        Main.OutputData outputData = Main.firstComeFirstServedScheduler(inputData);
        List<Main.Process> processes = outputData.getProcesses();
        assertEquals(11, processes.get(0).getTurnaroundTime());
        assertEquals(13, processes.get(1).getTurnaroundTime());
        assertEquals(16, processes.get(2).getTurnaroundTime());
        assertEquals(18, processes.get(3).getTurnaroundTime());
        assertEquals(24, processes.get(4).getTurnaroundTime());
        assertEquals(1, processes.get(0).getWaitingTime());
        assertEquals(12, processes.get(1).getWaitingTime());
        assertEquals(14, processes.get(2).getWaitingTime());
        assertEquals(17, processes.get(3).getWaitingTime());
        assertEquals(19, processes.get(4).getWaitingTime());
    }

    @Test
    void testFcfs2() throws URISyntaxException, IOException {
        Main.InputData inputData = Main.parseInputFile(getPathForResourceFile("/datafile2.txt"));
        Main.OutputData outputData = Main.firstComeFirstServedScheduler(inputData);
        List<Main.Process> processes = outputData.getProcesses();
        assertEquals(11, processes.get(0).getTurnaroundTime());
        assertEquals(11, processes.get(1).getTurnaroundTime());
        assertEquals(10, processes.get(2).getTurnaroundTime());
        assertEquals(8, processes.get(3).getTurnaroundTime());
        assertEquals(10, processes.get(4).getTurnaroundTime());
        assertEquals(1, processes.get(0).getWaitingTime());
        assertEquals(10, processes.get(1).getWaitingTime());
        assertEquals(8, processes.get(2).getWaitingTime());
        assertEquals(7, processes.get(3).getWaitingTime());
        assertEquals(5, processes.get(4).getWaitingTime());
    }

    @Test
    void measureFcfs() throws URISyntaxException, IOException, InterruptedException {
        Thread.sleep(500);
        Main.InputData inputData = Main.parseInputFile(getPathForResourceFile("/datafile2.txt"));
        for (int i = 0; i < 100000; i++) {
            Main.firstComeFirstServedScheduler(inputData);
        }
    }

    @Test
    void measureRr() throws URISyntaxException, IOException, InterruptedException {
        Thread.sleep(500);
        Main.InputData inputData = Main.parseInputFile(getPathForResourceFile("/datafile2.txt"));
        for (int i = 0; i < 100000; i++) {
            Main.roundRobinScheduler(inputData);
        }
    }

    @Test
    void measureNrr() throws URISyntaxException, IOException, InterruptedException {
        Thread.sleep(500);
        Main.InputData inputData = Main.parseInputFile(getPathForResourceFile("/datafile2.txt"));
        for (int i = 0; i < 100000; i++) {
            Main.nrrScheduler(inputData);
        }
    }
}