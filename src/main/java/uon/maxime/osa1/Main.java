package uon.maxime.osa1;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.*;

public class Main {

    private static final String BEGIN = "BEGIN";
    private static final String DISP = "DISP";
    private static final String END = "END";
    private static final String ID = "ID";
    private static final String ARRIVE = "Arrive";
    private static final String EXEC_SIZE = "ExecSize";
    private static final String EOF = "EOF";
    private static final String HEADER = "header";
    private static final String PROCESS = "process";
    private static final String IDLE = "IDLE";
    private static final String TABLE_PROCESS = "Process ";
    private static final String TABLE_TURNAROUND_TIME = "Turnaround Time ";
    private static final String TABLE_WAITING_TIME = "Waiting Time";
    private static final String TABLE_ALGORITHM = "Algorithm ";
    private static final String TABLE_AVG_TURNAROUND_TIME = "Average Turnaround Time ";
    private static final String TABLE_AVG_WAITING_TIME = "Average Waiting Time";

    public static class Process {
        /**
         * Process name.
         */
        private final String id;
        /**
         * Time step at which the process should enter the ready queue.
         */
        private final int arrivalTime;
        /**
         * Total time required for this process to be completed.
         */
        private final int requiredServiceTime;

        /**
         * Time this process has been running.
         */
        private int currentServiceTime;
        /**
         * Time that was needed in total to completely execute this process.
         */
        private int turnaroundTime;
        /**
         * Time this process spent in the ready queue, waiting for being executed.
         */
        private int waitingTime;

        public Process(String id, int arrivalTime, int requiredServiceTime) {
            if (id == null) // Just to be sure.
                throw new NullPointerException("Process::ctor id cannot be null.");
            this.id = id;
            this.arrivalTime = arrivalTime;
            this.requiredServiceTime = requiredServiceTime;
            this.currentServiceTime = 0;
        }

        public Process(Process other) {
            this.id = other.id;
            this.arrivalTime = other.arrivalTime;
            this.requiredServiceTime = other.requiredServiceTime;
            this.currentServiceTime = other.currentServiceTime;
            this.turnaroundTime = other.turnaroundTime;
            this.waitingTime = other.waitingTime;
        }

        public int getTurnaroundTime() {
            return turnaroundTime;
        }

        public int getWaitingTime() {
            return waitingTime;
        }

        /**
         * @param obj Other object to check that we're equal to.
         * @return True if we have member-wise equality.
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Process other = (Process) obj;
            return this.id.equals(other.id)
                    && this.arrivalTime == other.arrivalTime
                    && this.requiredServiceTime == other.requiredServiceTime
                    && this.currentServiceTime == other.currentServiceTime
                    && this.turnaroundTime == other.turnaroundTime
                    && this.waitingTime == other.waitingTime;
        }

        /**
         * For debugging / unit testing purposes.
         *
         * @return String representation of this process.
         */
        @Override
        public String toString() {
            return "(" + this.id + ", " + this.arrivalTime + ", " + this.requiredServiceTime + ")";
        }
    }

    /**
     * Contains information about the processes to execute, and the time needed to
     * run the dispatcher.
     */
    public static class InputData {
        private final int dispatcherRunningTime;
        private final Queue<Process> processes;

        public InputData(int dispatcherRunningTime, List<Process> processes) {
            if (processes == null) // Just to be sure.
                throw new NullPointerException("InputData::ctor processes cannot be null.");

            this.dispatcherRunningTime = dispatcherRunningTime;
            // Sort the processes by arrival time, just in case the input file was
            // badly sorted.
            processes.sort(Comparator.comparingInt(o -> o.arrivalTime));
            this.processes = new LinkedList<>();
            this.processes.addAll(processes);
        }

        public int getDispatcherRunningTime() {
            return dispatcherRunningTime;
        }

        public Queue<Process> getProcesses() {
            return processes;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            InputData other = (InputData) obj;
            return this.dispatcherRunningTime == other.dispatcherRunningTime
                    && this.processes.equals(other.processes);
        }
    }

    /**
     * Scheduling algorithms generate instances of this class, used to describe
     * in what order the different processes were executed.
     */
    public static class OutputData {
        /**
         * Stores which process is being executed at each time step. Not
         * really necessary but helped for debugging. A more compact way
         * of storing this could be done.
         */
        private final List<String> timeline;
        /**
         * List of the processes that were successfully entirely executed
         * by the scheduling algorithm.
         */
        private final List<Process> processes;
        /**
         * Name of the scheduling algorithm used to generate this output
         * data.
         */
        private final String schedulerName;

        public OutputData(String schedulerName) {
            timeline = new ArrayList<>();
            this.processes = new ArrayList<>();
            this.schedulerName = schedulerName;
        }

        public List<Process> getProcesses() {
            return processes;
        }

        /**
         * Adds a process to the output list. Processes are added when they
         * finished being executed. The dispatcher process is never added.
         *
         * @param process Process to add.
         */
        private void addProcess(Process process) {
            if (!DISP.equals(process.id)) {
                processes.add(process);
            }
        }

        /**
         * Print the output data generated by a given scheduler in the format
         * specified in the assignment.
         */
        private void print() {
            System.out.println(this.schedulerName + ":");

            // Sort the processes by name. Ugly but when printing we don't need
            // the "original" order (ie the order in which processes are finished)
            // anymore.
            processes.sort(Comparator.comparing(o -> o.id));
            // Print the compact timeline.
            String previousOperation = "";
            for (int i = 0; i < timeline.size(); i++) {
                String operation = timeline.get(i);
                if (DISP.equals(operation)
                        || IDLE.equals(operation)
                        || operation.equals(previousOperation))
                    continue;
                System.out.println("T" + i + ": " + operation);

                previousOperation = operation;
            }
            // Skip a line after the timeline.
            System.out.println();

            // Print the statistics table.
            System.out.println(TABLE_PROCESS + TABLE_TURNAROUND_TIME + TABLE_WAITING_TIME);
            for (Process p : processes) {
                System.out.print(p.id + " ".repeat(TABLE_PROCESS.length() - p.id.length()));
                String turnaroundTimeStr = String.valueOf(p.turnaroundTime);
                System.out.print(turnaroundTimeStr + " ".repeat(TABLE_TURNAROUND_TIME.length() - turnaroundTimeStr.length()));
                System.out.println(p.waitingTime);
            }

            // Skip a line after the table.
            System.out.println();
        }

        private float getAverageTurnaroundTime() {
            float avg = 0.0f;
            for (Process process : processes) {
                avg += process.turnaroundTime;
            }
            return avg / processes.size();
        }

        private float getAverageWaitingTime() {
            float avg = 0.0f;
            for (Process process : processes) {
                avg += process.waitingTime;
            }
            return avg / processes.size();
        }
    }

    /**
     * Reads a token from the scanner and ensures it is the same as the expected string. If
     * not, a {@link RuntimeException} is thrown.
     *
     * @param scanner      Scanner to read the token from.
     * @param expected     String we want the token to be equal to.
     * @param filePosition String used for error reporting, to say if the issue occurred in a
     *                     wrongly specified process or in the header.
     */
    private static void ensureToken(Scanner scanner, String expected, String filePosition) {
        String token = scanner.next();
        if (!expected.equals(token)) {
            throw new RuntimeException("Badly formatted input file (" + filePosition
                    + "): expected " + expected + " keyword, got " + token + ".");
        }
    }

    /**
     * Ensures the argument is equal to the  a {@link Main#ID} token. If not,
     * a {@link RuntimeException} is thrown.
     *
     * @param token Token to check.
     */
    private static void ensureIdToken(String token) {
        if (!Main.ID.equals(token)) {
            throw new RuntimeException("Badly formatted input file (" + Main.PROCESS
                    + "): expected " + Main.ID + " keyword, got " + token + ".");
        }
    }

    /**
     * Reads the input file and generates an object containing this data in a
     * ready-to-process format.
     *
     * @param filePath Path to the file to read.
     * @return {@link InputData} object containing the process information and
     * the dispatcher time.
     * @throws IOException If an error occurs when reading the file.
     */
    public static InputData parseInputFile(String filePath) throws IOException {
        try (InputStream fileStream = new FileInputStream(filePath)) {
            // Skip whitespaces, new lines, etc, and the : delimiter.
            // Note on the regex: after a lot of suffering, I came to realize the regex needs
            // to match for the WHOLE delimiter, hence the '+'. Otherwise something such as
            // ' :' would not be considered as a single delimiter, making parsing harder.
            Scanner scanner = new Scanner(fileStream).useDelimiter("(\r\n|\n|\\s|:)+");

            // 1. BEGIN keyword.
            ensureToken(scanner, BEGIN, HEADER);

            // 2. DISP integer END.
            ensureToken(scanner, DISP, HEADER);
            int dispatcherRunningTime = scanner.nextInt();
            ensureToken(scanner, END, HEADER);

            List<Process> processes = new ArrayList<>();
            while (true) {
                String nextToken = scanner.next();
                if (EOF.equals(nextToken))
                    break;
                ensureIdToken(nextToken); // ID.
                String processName = scanner.next();
                ensureToken(scanner, ARRIVE, PROCESS);
                int arrivalTime = scanner.nextInt();
                ensureToken(scanner, EXEC_SIZE, PROCESS);
                int execSize = scanner.nextInt();
                ensureToken(scanner, END, PROCESS);
                processes.add(new Process(processName, arrivalTime, execSize));
            }

            // Note: the EOF token is parsed in the loop above.
            return new InputData(dispatcherRunningTime, processes);
        }
    }

    /**
     * Performs a DEEP copy of the process collection. Just copying the collections does
     * not work, since only the references get copied. Actual copies of the processes
     * themselves are needed.
     *
     * @param original      Original process collection.
     * @param newCollection Process collection to copy the processes into.
     */
    public static void cloneProcesses(Collection<Process> original, Collection<Process> newCollection) {
        for (Process p : original) {
            newCollection.add(new Process(p));
        }
    }

    /**
     * FCFS scheduler. Executes the processes in the order in which they come.
     *
     * @param inputData Input data to work from: process list and dispatcher time.
     * @return {@link OutputData} containing the execution times, the timeline, etc.
     */
    public static OutputData firstComeFirstServedScheduler(InputData inputData) {
        OutputData outputData = new OutputData("FCFS");

        int currentTime = 0;
        Process dispatcherProcess = new Process(DISP, -1, inputData.dispatcherRunningTime);
        Queue<Process> allProcesses = new LinkedList<>();
        cloneProcesses(inputData.processes, allProcesses);
        Queue<Process> readyQueue = new LinkedList<>();
        Process currentProcess = null;
        while (!(allProcesses.isEmpty() && readyQueue.isEmpty()) || currentProcess != null) {
            // When a process arrives, it goes in the ready queue.
            while (!allProcesses.isEmpty() && allProcesses.peek().arrivalTime <= currentTime) {
                readyQueue.add(dispatcherProcess); // Invoke the dispatcher in between each process.
                readyQueue.add(allProcesses.poll());
            }
            // If there is no current process (idle processor), and there
            // is at least one process waiting in the ready queue, just put
            // the first item of the queue as active.
            if (currentProcess == null && !readyQueue.isEmpty()) {
                currentProcess = readyQueue.poll();
                // This needs to be reset at 0 every time for the dispatcher process.
                // We can do this blindly for all processes, since each process is set as the current process only
                // once. In other schedulers, this is done only for the dispatcher process.
                currentProcess.currentServiceTime = 0;
            }
            // For all processes in the ready queue (ie waiting), increment their current waiting
            // time.
            readyQueue.forEach(process -> process.waitingTime++);
            // If we have a process currently running, increment its running
            // time.
            if (currentProcess != null) {
                currentProcess.currentServiceTime++;
                outputData.timeline.add(currentProcess.id);
                // If we have finished executing the current process by executing
                // this unit of time, set the current process as null.
                if (currentProcess.currentServiceTime == currentProcess.requiredServiceTime) {
                    // Note: +1 at the end because the time incrementation is done at the end of the loop.
                    currentProcess.turnaroundTime = currentTime - currentProcess.arrivalTime + 1;
                    outputData.addProcess(currentProcess);
                    currentProcess = null;
                }
            } else {
                outputData.timeline.add(IDLE);
            }
            currentTime++;
        }

        return outputData;
    }

    private static final int RR_QUANTUM = 4;

    /**
     * RR scheduler. Based on the FCFS implementation with the difference that processes
     * get put back at the beginning of a queue if they were not fully executed at the end
     * of their time quantum.
     *
     * @param inputData Input data to work from: process list and dispatcher time.
     * @return {@link OutputData} containing the execution times, the timeline, etc.
     */
    public static OutputData roundRobinScheduler(InputData inputData) {
        OutputData outputData = new OutputData("RR");

        int currentTime = 0;
        int currentQuantum = 0;
        Process dispatcherProcess = new Process(DISP, -1, inputData.dispatcherRunningTime);
        Queue<Process> allProcesses = new LinkedList<>();
        cloneProcesses(inputData.processes, allProcesses);
        Queue<Process> readyQueue = new LinkedList<>();
        // HashSet<Process> wipProcesses = new HashSet<>();
        Process currentProcess = null;
        while (!(allProcesses.isEmpty() && readyQueue.isEmpty()) || currentProcess != null) {
            // When a process arrives, it goes in the ready queue.
            while (!allProcesses.isEmpty() && allProcesses.peek().arrivalTime <= currentTime) {
                readyQueue.add(dispatcherProcess); // Invoke the dispatcher in between each process.
                readyQueue.add(allProcesses.poll());
            }
            // If there is no current process (idle processor), and there
            // is at least one process waiting in the ready queue, just put
            // the first item of the queue as active.
            if (currentProcess == null && !readyQueue.isEmpty()) {
                currentProcess = readyQueue.poll();
                if (DISP.equals(currentProcess.id))
                    currentProcess.currentServiceTime = 0; // This needs to be reset at 0 every time for the dispatcher process.
            }
            // For all processes in the ready queue (ie waiting), increment their current waiting
            // time.
            readyQueue.forEach(process -> process.waitingTime++);
            // If we have a process currently running, increment its running
            // time.
            if (currentProcess != null) {
                currentProcess.currentServiceTime++;
                outputData.timeline.add(currentProcess.id);
                // If we have finished executing the current process by executing
                // this unit of time, set the current process as null.
                if (currentProcess.currentServiceTime == currentProcess.requiredServiceTime) {
                    outputData.addProcess(currentProcess);
                    // Note: +1 at the end because the time incrementation is done at the end of the loop.
                    currentProcess.turnaroundTime = currentTime - currentProcess.arrivalTime + 1;
                    currentProcess = null;
                    currentQuantum = 0; // Reset the quantum when changing process.
                } else {
                    // Increment the quantum IF THE CURRENT PROCESS IS NOT THE DISPATCHER. This is because
                    // the dispatcher must not be interrupted, it runs for however long it needs, it is
                    // not a normal process.
                    // If we're at the end of it, and if the current process
                    // is not null, put the current process back at the start of the ready queue.
                    if (currentProcess != dispatcherProcess) {
                        currentQuantum++;
                        if (currentQuantum == RR_QUANTUM) {
                            currentQuantum = 0;
                            // Only run the dispatcher if the ready queue isn't empty to avoid
                            // wasting CPU time.
                            if (!readyQueue.isEmpty()) {
                                readyQueue.add(dispatcherProcess);
                                readyQueue.add(currentProcess);
                                currentProcess = null;
                            }
                        }
                    }
                }
            } else {
                outputData.timeline.add(IDLE);
            }
            currentTime++;
        }

        return outputData;
    }

    private static final int NRR_MIN_QUANTUM = 2;

    /**
     * NRR scheduler. Based on the RR implementation with the difference that processes
     * have individual time quanta, managed through a hash map. Those quanta diminish
     * every time a process becomes active, up to a minimum of {@link Main#NRR_MIN_QUANTUM}.
     *
     * @param inputData Input data to work from: process list and dispatcher time.
     * @return {@link OutputData} containing the execution times, the timeline, etc.
     */
    public static OutputData nrrScheduler(InputData inputData) {
        OutputData outputData = new OutputData("NRR");
        HashMap<String, Integer> processQuanta = new HashMap<>();
        int currentTime = 0;
        int currentQuantum = 0;
        Process dispatcherProcess = new Process(DISP, -1, inputData.dispatcherRunningTime);
        Queue<Process> allProcesses = new LinkedList<>();
        cloneProcesses(inputData.processes, allProcesses);
        Queue<Process> readyQueue = new LinkedList<>();
        // HashSet<Process> wipProcesses = new HashSet<>();
        Process currentProcess = null;
        while (!(allProcesses.isEmpty() && readyQueue.isEmpty()) || currentProcess != null) {
            // When a process arrives, it goes in the ready queue.
            while (!allProcesses.isEmpty() && allProcesses.peek().arrivalTime <= currentTime) {
                readyQueue.add(dispatcherProcess); // Invoke the dispatcher in between each process.
                Process arrivedProcess = allProcesses.poll();
                readyQueue.add(arrivedProcess);
                assert arrivedProcess != null; // Normally never happens unless the code is wrong. I just put that for an IntelliJ warning.
                processQuanta.put(arrivedProcess.id, RR_QUANTUM);
            }
            // If there is no current process (idle processor), and there
            // is at least one process waiting in the ready queue, just put
            // the first item of the queue as active.
            if (currentProcess == null && !readyQueue.isEmpty()) {
                currentProcess = readyQueue.poll();
                if (DISP.equals(currentProcess.id))
                    currentProcess.currentServiceTime = 0; // This needs to be reset at 0 every time for the dispatcher process.
            }
            // For all processes in the ready queue (ie waiting), increment their current waiting
            // time.
            readyQueue.forEach(process -> process.waitingTime++);
            // If we have a process currently running, increment its running
            // time.
            if (currentProcess != null) {
                currentProcess.currentServiceTime++;
                outputData.timeline.add(currentProcess.id);
                // If we have finished executing the current process by executing
                // this unit of time, set the current process as null.
                if (currentProcess.currentServiceTime == currentProcess.requiredServiceTime) {
                    outputData.addProcess(currentProcess);
                    // Note: +1 at the end because the time incrementation is done at the end of the loop.
                    currentProcess.turnaroundTime = currentTime - currentProcess.arrivalTime + 1;
                    processQuanta.remove(currentProcess.id); // No need to keep track of this process's quantum anymore.
                    currentProcess = null;
                    currentQuantum = 0; // Reset the quantum when changing process.
                } else {
                    // Increment the quantum IF THE CURRENT PROCESS IS NOT THE DISPATCHER. This is because
                    // the dispatcher must not be interrupted, it runs for however long it needs, it is
                    // not a normal process.
                    // If we're at the end of it, and if the current process
                    // is not null, put the current process back at the start of the ready queue.
                    if (currentProcess != dispatcherProcess) {
                        currentQuantum++;
                        int processQuantum = processQuanta.get(currentProcess.id);
                        if (currentQuantum == processQuantum) {
                            currentQuantum = 0;
                            // Only run the dispatcher if the ready queue isn't empty to avoid
                            // wasting CPU time.
                            if (!readyQueue.isEmpty()) {
                                readyQueue.add(dispatcherProcess);
                                readyQueue.add(currentProcess);
                                processQuanta.put(currentProcess.id, Math.max(NRR_MIN_QUANTUM, processQuantum - 1));
                                currentProcess = null;
                            }
                        }
                    }
                }
            } else {
                outputData.timeline.add(IDLE);
            }
            currentTime++;
        }

        return outputData;
    }

    /**
     * Prints a summary of the performance of each scheduling algorithm in the given
     * list. Results are displayed in a table following the specifications of given
     * in the assignment.
     *
     * @param outputData Output of the different scheduling algorithms.
     */
    private static void printSummary(List<OutputData> outputData) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);

        System.out.println("Summary");
        System.out.println(TABLE_ALGORITHM + TABLE_AVG_TURNAROUND_TIME + TABLE_AVG_WAITING_TIME);
        for (OutputData results : outputData) {
            System.out.print(results.schedulerName + " ".repeat(TABLE_ALGORITHM.length() - results.schedulerName.length()));
            String avgTurnaroundTimeStr = df.format(results.getAverageTurnaroundTime());
            System.out.print(avgTurnaroundTimeStr + " ".repeat(TABLE_AVG_TURNAROUND_TIME.length() - avgTurnaroundTimeStr.length()));
            System.out.println(df.format(results.getAverageWaitingTime()));
        }

    }

    public static void main(String[] args) {
        System.out.println("---- OS Assignment 1 ----");

        // Ensure we have a file to work from.
        if (args.length < 1) {
            System.out.println("Missing source file path.");
            System.out.println("Usage:");
            System.out.println("java -jar osa1.jar INPUT_PATH");
            return;
        }

        // Parse the input file: read the processes, dispatcher running time etc.
        final String filePath = args[0];
        try {
            InputData inputData = parseInputFile(filePath);
            // "Output should be strictly in the order FCFS, RR, NRR, --FB--, Summary"
            List<OutputData> results = new ArrayList<>();
            results.add(firstComeFirstServedScheduler(inputData));
            results.add(roundRobinScheduler(inputData));
            results.add(nrrScheduler(inputData));
            results.forEach(OutputData::print);
            printSummary(results);
        } catch (IOException e) {
            System.out.println("IO error occurred while parsing the input file. Details:");
            e.printStackTrace();
        }
    }
}
